import mymix from  './base-model.js'
export default {
  extends  : mymix,

  data : function()
  {
    return {
      items : [],
      showModal : false,
      formMode :  'new',
      formConfigurations : [],
      form : {},
      formErrors : [],
      relationshipObjs : [],
      filterObj : {
          type : '',
          value : ''
      },
      isLoading : false
    };
  },
  methods : {
      filterAll : function()
      {
          this.pageVar  = 1;
          this.clearFilters().setFilters(this.filterObj.type+":"+this.filterObj.value);
          this.loadItems();
      },
      clearAndFilter : function()
      {
          this.pageVar  = 1;
          this.filterObj = {
                type : '',
                value : ''
          };
          this.clearFilters().loadItems();
      },
      loadData : function()
      {
          this.pageVar = this.pagination.current_page;
          this.loadItems();
      },
    loadItems : function()
    {
      var self = this;
      self.isLoading = true;
      this.all().then(function(){
          self.items = self.response.data;
          self.isLoading = false;
      });
    },
    showAddForm : function(){
      this.formMode = 'new';
      this.form = {};
      this.showModal = true;
    },
    showEditFormByObject : function(formObj)
    {
      this.formMode = 'edit';
      this.form = this.copy(formObj);
      var self = this;
      this.relationshipObjs.forEach(function(relObj){
          if(typeof self.form[relObj] !== "undefined")
          {
              delete self.form[relObj];
          }
      });

      console.log(this.form);
      this.showModal = true;
    },
    showEditForm : function(index)
    {
      this.formMode = 'edit';
      this.form = this.copy(this.items[index]);
      var self = this;
      this.relationshipObjs.forEach(function(relObj){
          if(typeof self.form[relObj] !== "undefined")
          {
              delete self.form[relObj];
          }
      });
      console.log(this.relationshipObjs);
      console.log(this.form);
      this.showModal = true;
    },
    saveForm : function()
    {
      console.log(this.form);
      var self = this;
      if(this.formMode === 'new'){
        this.store(this.form).then(function(){
            if(self.errorResponse === null){
                self.all().then(function(){
                    self.showModal = false;
                    self.items = self.response.data;
                });
            }
        });
      }else{
        this.update(this.form.id, this.form).then(function(response){
            if(self.errorResponse === null){
                self.all().then(function(){
                    self.showModal = false;
                    self.items = self.response.data;
                });
            }
        });
      }
    },
    deleteRecord : function(index)
    {
        var self = this;
        this.remove(this.items[index].id)
        .then(function(response){
            console.log(response);
            self.items.splice(index, 1);
        });
    }
  }
}
