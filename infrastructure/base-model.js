export default {
    created: function () {
        console.log('MIXIN CREATED');
      },
    data: function()
    {
        return {
            resourceName : '',
            selectedItem : null,
            response : null,
            filters : null,
            rootName : '/api/',
            pageVar : 1,
            paginate : false,
             pagination: {
                total: 2,
                per_page: 10,    // required
                current_page: 1, // required
                last_page: 2,    // required
                from: 1,
                to: 14
              },
              paginationOptions: {
                offset: 4,
                previousText: 'Prev',
                nextText: 'Next',
                alwaysShowPrevNext: true
              },
              errorResponse : null
        };
    },
    methods: {
      copy : function(data)
      {
          return JSON.parse(JSON.stringify(data));
      },
      customRequest : function(requestType, url, data)
      {
            var self = this;
//            return axios
            return axios({
              method: requestType,
              url: url,
              data: data
            });
      },
      get: function (id) {
          var self = this;
            return axios.get(this.rootName+this.resourceName+"/"+id).then(function(response){
                  self.response = response.data;
                  self.selectedItem = response.data.data;
            })
            .catch(function (error) {
              console.log(error);
            });
      },
      clearFilters : function(){
          this.filters= null;
          return this;
      },
      setFilters : function(filters)
      {
           if(this.filters === null){
               this.filters = '?fields='+filters;
           }else{
                this.filters += ','+filters;
            }
            return this;
      },
      all: function () {
          console.log("ALL");
        var self =this;
        var url = this.rootName +this.resourceName;
        if(this.filters !== null)
        {
            url += this.filters+"&page="+this.pageVar;
        }else{
            url += "?page="+this.pageVar;
        }
        if(this.paginate === true)
        {
            url += "&paginate=true";
        }
        return axios.get(url).then(function(response){
            if(self.paginate===true)
            {
                self.pagination.current_page = response.data.current_page;
                self.pagination.per_page = response.data.per_page;
                self.pagination.last_page = response.data.last_page;
            }
            self.response = response.data;
        })
        .catch(function (error) {
          console.log(error);
        });
      },
      store : function(data){
        var self =this;
        
        self.errorResponse = null;
        return axios.post(this.rootName + this.resourceName, data).then(function(response){
            self.response = response.data;
            self.selectedItem = response.data.data;
        })
        .catch(function (error) {
            console.log(error.response.data);
            self.errorResponse = error.response.data;
        });
      },
      update : function(id, data){
        var self =this;
        
        self.errorResponse = null;
        return axios.put(this.rootName + this.resourceName+"/"+id, data).then(function(response){
            self.response = response.data;
            self.selectedItem = response.data.data;
        })
        .catch(function (error) {
            console.log(error);
            self.errorResponse = error.response.data;
        });
      },
      remove : function(id)
      {
        var self =this;
        return axios.delete(this.rootName + this.resourceName+"/"+id)
        .catch(function (error) {
          console.log(error);
        });
      }
    }
}
