'use strict';

require('./components/utils/array_functions');
require('./infrastructure/jtk');
Vue = require('vue');

require('./components/utils/dhivehiBox');

Vue.component('ed-dialog', require('./components/utils/Dialog.vue'));
Vue.component('ed-pagination', require('./components/utils/PaginateComponent.vue'));
Vue.component('ed-breadcrumb', require('./components/utils/BreadCrumb.vue'));
Vue.component('form-gen', require('./components/utils/form-gen/FormGen.vue'));
Vue.component('view-gen', require('./components/utils/view-gen/ViewGen.vue'));
Vue.component('default-list', require('./components/default-component/DefaultListComponent.vue'));
Vue.component('default-view', require('./components/default-component/DefaultViewComponent.vue'));

require('./components/utils/Select2.js');

module.exports = {
	
}