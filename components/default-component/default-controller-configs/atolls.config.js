export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'name'
       }
    ],
    relationshipObjs : ['islands'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Title',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          {
          title : 'Islands',
          relType : 'hasMany',
          mapToObj : 'islands',
          resourceName : 'islands',
          displayFields : ['name'],
          displayLabels : ['Name'],
          dataMappingDisplay : {
            is_correct : {
              type : 'boolean',
              callback : function(val){
                if(val){
                  return 'Correct';
                }
                return 'Not Correct';
              }
            }
          },
          routeName : 'default-view',
          formConfigRelatedField : 'atoll_id',
          formConfig : require('./islands.config').default.form
        }
      ],
      hiddenFields : [ 'islands', 'created_at', 'updated_at']
    }
}
