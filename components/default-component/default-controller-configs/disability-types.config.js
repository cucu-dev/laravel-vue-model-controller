export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'title'
       }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'title',
             colDisplayName : 'Title',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
        
      ],
      hiddenFields : [  'created_at', 'updated_at']
    }
}
