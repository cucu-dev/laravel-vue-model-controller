export default {
    route : 'v1/entries',
    viewRouteResourceName:  'entry-view', 
    form :  [
       {
            label : 'NID',
            type : 'input:text',
            formModel : 'nid'
       },
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Name Dhivehi',
            type : 'input:dhivehi',
            formModel : 'name_dh'
       },
       {
            label : 'Email',
            type : 'input:text',
            formModel : 'email'
       },
       {
            label : 'Mobile Number',
            type : 'input:text',
            formModel : 'mobile_number'
       },
       {
            label : 'Island',
            type : 'input:text',
            formModel : 'island'
       },
       {
             label: "Program Type",
             type: 'select2',
             formModel: 'program_type_id',
             selectOptions: {
                 option: 'id',
                 value: 'title_en'
             },
             dataType: 'api',
             dataApi: '/api/program-types',
             dataArray: []
         }
    ],
    relationshipObjs : ['program_type', 
        'created_by', 'missing_documents'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'entry_number',
             colDisplayName : 'Entry Number',
             className : ''
            },
            {
             colName : 'nid',
             colDisplayName : 'NID',
             className : ''
            },
            {
             colName : 'program_type',
             colDisplayName : 'Program Type',
             className : '',
             colNameFunc : function(data)
             {
                 return data.title_en;
             }
            },
            {
             colName : 'created_at',
             colDisplayName : 'Created At',
             className : ''
            },
          {
             colName : 'mobile_number',
             colDisplayName : 'Mobile Number',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'program_type',
            displayField : 'title_en',
            displayLabel : 'Program Type',
            routeName : 'default-view'
          }
      ],
      hiddenFields : ['program_type_id', 'created_at', 'updated_at']
    }
}
