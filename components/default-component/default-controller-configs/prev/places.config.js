export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },{
            label : 'Address',
            type : 'input:text',
            formModel : 'address'
       },{
             label: "Organization",
             type: 'select',
             formModel: 'organization_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/organizations',
             dataArray: []
         }
    ],
    relationshipObjs : ['organization'],
    view : {
      fileMappings : {
          
      },
      listFields : [
            {
                colName : 'id',
                colDisplayName : 'ID',
                className : ''
            },
            {
                colName : 'name',
                colDisplayName : 'Name',
                className : ''
            },
            {
                colName : 'organization',
                colDisplayName : 'Organization',
                className : '',
                colNameFunc: function(data)
                {
                    return data.name;
                }
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'organization',
            displayField : 'name',
            displayLabel : 'Organization',
            routeName : 'default-view'
          }
      ],
      hiddenFields : ['organization', 'created_at', 'updated_at']
    }
}
