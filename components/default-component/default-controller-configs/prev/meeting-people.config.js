export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },{
             label: "Island",
             type: 'select',
             formModel: 'island_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/islands',
             dataArray: []
         }
    ],
    relationshipObjs : ['island'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'island',
             colDisplayName : 'Island',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'island',
            displayField : 'name',
            displayLabel : 'Island',
            routeName : 'default-view'
          }
      ],
      hiddenFields : ['island', 'created_at', 'updated_at']
    }
}
