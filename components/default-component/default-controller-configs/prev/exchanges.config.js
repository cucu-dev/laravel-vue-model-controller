export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },{
             label: "Site",
             type: 'select',
             formModel: 'site_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/sites',
             dataArray: []
         }
    ],
    relationshipObjs : ['site'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'site',
             colDisplayName : 'Site',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'site',
            displayField : 'name',
            displayLabel : 'Site',
            routeName : 'default-view'
          }
      ],
      hiddenFields : ['site', 'created_at', 'updated_at']
    }
}
