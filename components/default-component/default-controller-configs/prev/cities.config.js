export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label: "Is Active",
            type: 'select',
            formModel: 'is_active',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id: 1,
                    title: 'Yes'
                },
                {
                    id: 0,
                    title: 'No'
                }
            ]
        },{
             label: "Country",
             type: 'select',
             formModel: 'country_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/countries',
             dataArray: []
         }
    ],
    relationshipObjs : ['country', 'created_by', 'program_type'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'country',
             colDisplayName : 'Country',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            },
          {
             colName : 'is_active',
             colDisplayName : 'Is Active',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'country',
            displayField : 'name',
            displayLabel : 'Country',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'country','created_at', 'updated_at']
    }
}
