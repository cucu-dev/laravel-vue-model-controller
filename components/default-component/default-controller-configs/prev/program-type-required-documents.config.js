export default {
    form :  [
       {
            label : 'Title(English)',
            type : 'input:text',
            formModel : 'title_en'
       },
       {
            label : 'Title(Dhivehi)',
            type : 'input:dhivehi',
            formModel : 'title_dh'
       },
       {
            label: "Is Active",
            type: 'select',
            formModel: 'is_active',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id: 1,
                    title: 'Yes'
                },
                {
                    id: 0,
                    title: 'No'
                }
            ]
        },{
             label: "Program Type",
             type: 'select2',
             formModel: 'program_type_id',
             selectOptions: {
                 option: 'id',
                 value: 'title_en'
             },
             dataType: 'api',
             dataApi: '/api/program-types',
             dataArray: []
         }
    ],
    relationshipObjs : ['created_by', 'program_type'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'title_en',
             colDisplayName : 'Title English',
             className : ''
            },
          {
             colName : 'title_dh',
             colDisplayName : 'Title Dhivehi',
             className : ''
            },
          {
             colName : 'is_active',
             colDisplayName : 'Is Active',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'program_type',
            displayField : 'title_en',
            displayLabel : 'Program Type',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'program_type_id', 'created_by', 'created_at', 'updated_at']
    }
}
