export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Atoll',
            type : 'input:text',
            formModel : 'atoll'
       },
       {
            label : 'Lat',
            type : 'input:text',
            formModel : 'lat'
       },
       {
            label : 'Lng',
            type : 'input:text',
            formModel : 'lng'
       }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'atoll',
             colDisplayName : 'Atoll',
             className : '',
             
            },
          {
             colName : 'lat',
             colDisplayName : 'Lat',
             className : ''
            },
          {
             colName : 'lng',
             colDisplayName : 'Lng',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          
      ],
      hiddenFields : ['created_at', 'updated_at']
    }
}
