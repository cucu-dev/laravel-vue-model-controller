export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label: "Is Active",
            type: 'select',
            formModel: 'is_active',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id: 1,
                    title: 'Yes'
                },
                {
                    id: 0,
                    title: 'No'
                }
            ]
        }
    ],
    relationshipObjs : ['cities'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Title',
             className : ''
            },
          {
             colName : 'is_active',
             colDisplayName : 'Is Active',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
          title : 'Cities',
          relType : 'hasMany',
          mapToObj : 'cities',
          resourceName : 'cities',
          displayFields : ['name'],
          displayLabels : ['Name'],
          dataMappingDisplay : {
            is_correct : {
              type : 'boolean',
              callback : function(val){
                if(val){
                  return 'Correct';
                }
                return 'Not Correct';
              }
            }
          },
          routeName : 'question-option-view',
          formConfigRelatedField : 'country_id',
          formConfig : require('./cities.config').default.form
        }
      ],
      hiddenFields : [ 'cities', 'created_at', 'updated_at']
    }
}
