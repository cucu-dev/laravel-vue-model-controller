export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'title'
       },
       {
            label : 'Summary',
            type : 'input:text',
            formModel : 'summary'
       },
       {
            label : 'Date Start',
            type : 'input:date',
            formModel : 'date_start'
       },
       {
            label : 'Date Start (Hours)',
            type : 'input:number',
            formModel : 'time_start_hours'
       },
       {
            label : 'Date Start (Minutes)',
            type : 'input:number',
            formModel : 'time_start_minutes'
       },
        {
            label : 'Date End',
            type : 'input:date',
            formModel : 'date_end'
       },
       {
            label : 'Date End (Hours)',
            type : 'input:number',
            formModel : 'time_end_hours'
       },
       {
            label : 'Date End (Minutes)',
            type : 'input:number',
            formModel : 'time_end_minutes'
       },
       
    {
             label: "Place",
             type: 'select',
             formModel: 'place_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/places',
             dataArray: []
         }
    ],
    relationshipObjs : ['place', 'people'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'title',
             colDisplayName : 'Title',
             className : ''
            },{
             colName : 'date_start',
             colDisplayName : 'Date Start',
             className : ''
            },{
             colName : 'date_end',
             colDisplayName : 'Date End',
             className : ''
            },
          {
             colName : 'place',
             colDisplayName : 'Place',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'place',
            displayField : 'name',
            displayLabel : 'Place',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'created_at', 'updated_at']
    }
}
