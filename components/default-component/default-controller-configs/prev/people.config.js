export default {
    form :  [
       {
            label : 'Card Number',
            type : 'input:text',
            formModel : 'card_number'
       },{
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },{
            label : 'Mobile Number',
            type : 'input:text',
            formModel : 'mobile_number'
       },{
            label : 'Email',
            type : 'input:text',
            formModel : 'email'
       },{
             label: "Organization",
             type: 'select',
             formModel: 'organization_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/organizations',
             dataArray: []
         }
    ],
    relationshipObjs : ['organization'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'mobile_number',
             colDisplayName : 'Mobile Number',
             className : ''
            },
          {
             colName : 'email',
             colDisplayName : 'Email',
             className : ''
            },
          {
             colName : 'organization',
             colDisplayName : 'Organization',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'organization',
            displayField : 'name',
            displayLabel : 'Organization',
            routeName : 'default-view'
          }
      ],
      hiddenFields : ['organization', 'created_at', 'updated_at']
    }
}
