export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Amount',
            type : 'input:text',
            formModel : 'amount'
       },
       {
            label: "Is Active",
            type: 'select',
            formModel: 'is_active',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id: 1,
                    title: 'Yes'
                },
                {
                    id: 0,
                    title: 'No'
                }
            ]
        }
        
    ],
    filerConfigs : [
        {
            'type' : 'text',
            'title' : 'Name',
            'name' : 'name'
        }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'amount',
             colDisplayName : 'Amount',
             className : ''
            },
          
          {
             colName : 'is_active',
             colDisplayName : 'Is Active',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
         
      ],
      hiddenFields : ['created_at', 'updated_at']
    }
}
