export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Company',
            type : 'input:text',
            formModel : 'company'
       },
       {
            label : 'Email',
            type : 'input:text',
            formModel : 'email'
       },
       {
            label : 'Contact Number',
            type : 'input:text',
            formModel : 'contact_number'
       },
       {
            label : 'Address',
            type : 'input:text',
            formModel : 'address'
       },
       {
            label: "Is Active",
            type: 'select',
            formModel: 'is_active',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id: 1,
                    title: 'Yes'
                },
                {
                    id: 0,
                    title: 'No'
                }
            ]
        },{
             label: "City",
             type: 'select2',
             formModel: 'city_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/cities',
             dataArray: []
         }
    ],
    relationshipObjs : ['city'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'email',
             colDisplayName : 'Email',
             className : ''
            },
          {
             colName : 'contact_number',
             colDisplayName : 'Contact Number',
             className : ''
            },
          {
             colName : 'city',
             colDisplayName : 'City',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            },
          {
             colName : 'is_active',
             colDisplayName : 'Is Active',
             className : ''
            }
        ],
      labelMappingDisplay : {
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'city',
            displayField : 'name',
            displayLabel : 'City',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'city','created_at', 'updated_at']
    }
}
