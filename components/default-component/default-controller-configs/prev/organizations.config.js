export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
       
      },
      relationshipMappingDisplay : [
      ],
      hiddenFields : [ 'created_at', 'updated_at']
    }
}
