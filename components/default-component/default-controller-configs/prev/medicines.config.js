export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Estimated Amount',
            type : 'input:dhivehi',
            formModel : 'estimated_amount'
       }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'estimated_amount',
             colDisplayName : 'Amount',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
        
      },
      relationshipMappingDisplay : [
        
      ],
      hiddenFields : ['created_at', 'updated_at']
    }
}
