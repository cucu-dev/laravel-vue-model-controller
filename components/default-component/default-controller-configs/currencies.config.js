export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Description',
            type : 'input:text',
            formModel : 'description'
       },
       {
            label : 'Symbol',
            type : 'input:text',
            formModel : 'symbol'
       },
       {
            label : 'Exchange Rate to USD',
            type : 'input:text',
            formModel : 'exchange_rate_to_usd'
       }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Title',
             className : ''
            },
          {
             colName : 'symbol',
             colDisplayName : 'Symbol',
             className : ''
            },
          {
             colName : 'exchange_rate_to_usd',
             colDisplayName : 'Exchange Rate to USD',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          
      ],
      hiddenFields : [  'created_at', 'updated_at']
    }
}
