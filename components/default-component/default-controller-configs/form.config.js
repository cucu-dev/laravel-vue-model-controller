export default {
    'atolls' : require('./atolls.config').default,
    'islands' : require('./islands.config').default,
    'disability-types' : require('./disability-types.config').default,
}
