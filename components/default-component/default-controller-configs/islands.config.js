export default {
    form :  [
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
             label: "Atoll",
             type: 'select',
             formModel: 'atoll_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/atolls',
             dataArray: []
         }
    ],
    relationshipObjs : ['atoll', 'created_by', 'program_type'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'atoll',
             colDisplayName : 'Atoll',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
        
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'atoll',
            mapToRoute : 'atolls',
            displayField : 'name',
            displayLabel : 'Atoll',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'atoll','created_at', 'updated_at']
    }
}
