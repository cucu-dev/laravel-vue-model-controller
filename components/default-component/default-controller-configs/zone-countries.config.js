export default {
    form :  [
       {
             label: "Country",
             type: 'select',
             formModel: 'country_id',
             selectOptions: {
                 option: 'id',
                 value: 'name'
             },
             dataType: 'api',
             dataApi: '/api/countries',
             dataArray: []
         }
    ],
    relationshipObjs : ['country', 'created_by', 'program_type'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'country',
             colDisplayName : 'Country',
             className : '',
             colNameFunc: function(data)
             {
                 return data.name;
             }
            }
        ],
      labelMappingDisplay : {
        
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'country',
            mapToRoute : 'countries',
            displayField : 'name',
            displayLabel : 'Country',
            routeName : 'default-view'
          }
      ],
      hiddenFields : [ 'country','created_at', 'updated_at']
    }
}
