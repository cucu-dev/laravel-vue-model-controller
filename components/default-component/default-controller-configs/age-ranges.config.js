export default {
    form :  [
       {
            label : 'Title',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Age Start',
            type : 'input:number',
            formModel : 'start'
       },
       {
            label : 'Age End',
            type : 'input:number',
            formModel : 'end'
       },
       {
            label: "Type",
            type: 'select',
            formModel: 'type',
            selectOptions: {
                option: 'title',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    title: 'adult'
                },
                {
                    title: 'teen'
                },
                {
                    title: 'child'
                },
                {
                    title: 'infant'
                }
            ]
        }
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Title',
             className : ''
            },
          {
             colName : 'type',
             colDisplayName : 'Type',
             className : ''
            },
          {
             colName : 'start',
             colDisplayName : 'Start Age',
             className : ''
            },
          {
             colName : 'end',
             colDisplayName : 'End Age',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          
      ],
      hiddenFields : [  'created_at', 'updated_at']
    }
}
