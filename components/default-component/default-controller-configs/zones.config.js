export default {
    form :  [
       {
            label : 'Start Date',
            type : 'input:date',
            formModel : 'start'
       },
       {
            label : 'End Date',
            type : 'input:date',
            formModel : 'end'
       },
       {
            label : 'Name',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label: "Country Inclusion Status",
            type: 'select',
            formModel: 'type',
            selectOptions: {
                option: 'id',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    id : true,
                    title: 'Yes'
                },
                {
                    id : false,
                    title: 'No'
                },
            ]
        }
    ],
    relationshipObjs : [ 'created_by'],
    view : {
      fileMappings : {
          
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Name',
             className : ''
            },
          {
             colName : 'start',
             colDisplayName : 'Start',
             className : ''
            },
          {
             colName : 'end',
             colDisplayName : 'End',
             className : ''
            }
        ],
      labelMappingDisplay : {
        
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          {
          title : 'Countries',
          relType : 'hasMany',
          mapToObj : 'countries',
          resourceName : 'zone-countries',
          displayFields : ['country'],
          displayLabels : ['Country'],
          dataMappingDisplay : {
            country : {
                type : 'text',
                callback : function(val){
                    return val.name;
                }
            },
            is_correct : {
              type : 'boolean',
              callback : function(val){
                if(val){
                  return 'Correct';
                }
                return 'Not Correct';
              }
            }
          },
          routeName : 'zone-countries',
          formConfigRelatedField : 'zone_id',
          formConfig : require('./zone-countries.config').default.form
        }
      ],
      hiddenFields : [ 'created_at', 'updated_at']
    }
}
