export default {
    form :  [
       {
            label: "Type",
            type: 'select',
            formModel: 'type',
            selectOptions: {
                option: 'title',
                value: 'title'
            },
            dataType: 'array',
            dataArray: [
                {
                    title: 'facility'
                },
                {
                    title: 'amenity'
                }
            ]
        },
        {
            label : 'Title',
            type : 'input:text',
            formModel : 'name'
       },
       {
            label : 'Icon',
            type : 'file',
            formModel : 'icon'
       }
       
    ],
    relationshipObjs : [],
    view : {
      fileMappings : {
          icon : {}
      },
      listFields : [
          {
             colName : 'id',
             colDisplayName : 'ID',
             className : ''
            },
          {
             colName : 'name',
             colDisplayName : 'Title',
             className : ''
            },
          {
             colName : 'type',
             colDisplayName : 'Type',
             className : ''
            }
        ],
      labelMappingDisplay : {
          
      },
      dataMappingDisplay : {
          
      },
      relationshipMappingDisplay : [
          
      ],
      hiddenFields : [  'created_at', 'updated_at']
    }
}
