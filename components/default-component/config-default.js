export default {
    form :  [
        {
             label: "Section",
             type: 'select2',
             formModel: 'section_id',
             selectOptions: {
                 option: 'id',
                 value: 'title'
             },
             dataType: 'api',
             dataApi: '/api/sections',
             dataArray: []
         },
       {
         label : 'Title',
         type : 'input:text',
         formModel : 'title'
       },
       {
         label : 'Image',
         type : 'file',
         formModel : 'image'
       },
       {
         label : 'DOB',
         type : 'input:date',
         formModel : 'dob'
       },
       {
         label : 'Extension',
         type : 'input:text',
         formModel : 'ext'
       },
       {
         label : 'Designation',
         type : 'input:text',
         formModel : 'designation'
       },
       {
         label : 'Mobile Number',
         type : 'input:text',
         formModel : 'mobile_number'
       },
       {
         label : 'Email',
         type : 'input:text',
         formModel : 'email'
       },
       {
         label: "Gender",
         type: 'select',
         formModel: 'gender',
         selectOptions: {
             option: 'id',
             value: 'title'
         },
         dataType: 'array',
         dataArray: [
             {
                 id: 'Male',
                 title: 'Male'
             },
             {
                 id: 'Female',
                 title: 'Female'
             }
         ]
      },
       {
         label: "Is Active",
         type: 'select',
         formModel: 'is_active',
         selectOptions: {
             option: 'id',
             value: 'title'
         },
         dataType: 'array',
         dataArray: [
             {
                 id: 1,
                 title: 'Yes'
             },
             {
                 id: 0,
                 title: 'No'
             }
         ]
      }
    ],
    relationshipObjs : ['leaves', 'section', 'image', 'created_by'],
    view : {
      fileMappings : {
          image : {
              type : 'file'
          }
      },
      labelMappingDisplay : {
          question_count : 'Question Count',
          is_active : 'Is Active'
      },
      dataMappingDisplay : {
        is_active : {
          type : 'boolean',
          callback : function(val){
            if(val){
              return 'Active';
            }
            return 'Not Active';
          }
        }
      },
      relationshipMappingDisplay : [
          {
            relType : 'belongsTo',
            mapToObj : 'section',
            displayField : 'title',
            displayLabel : 'Section',
            routeName : 'section-view'
          },
          {
          title : 'Leaves',
          relType : 'hasMany',
          mapToObj : 'leaves',
          resourceName : 'employee-leave',
          displayFields : ['from', 'to', 'type'],
          displayLabels : [ 'From', 'To', 'Type'],
          dataMappingDisplay : {
            
          },
          routeName : 'employee-leave-view',
          formConfig : require("./../employee-leaves/form.config").default.form
        },
      ],
      hiddenFields : ['created_by', 'created_at', 'updated_at']
    }
}
