Vue.component('dhivehiBox', {

    template: '<input type="text" class="thaana thaanaKeyboardInput" v-model="internalValue" />',

    props: ['value'],


    mounted(){
       
//        $('.thaana').thaana();
        thaanaKeyboard.init();
    },
    data: function() {
        return {
          internalValue: ''
        }
      }, 
    watch: {
    'internalValue': function() {
      // When the internal value changes, we $emit an event. Because this event is 
      // named 'input', v-model will automatically update the parent value
      this.$emit('input', this.internalValue);
    }
  },
   created: function() {
    // We initially sync the internalValue with the value passed in by the parent
    this.internalValue = this.value;
  }


});